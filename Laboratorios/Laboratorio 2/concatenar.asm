.data
buffer: .space 20
inicio: .asciiz "\nIntroduzca una cadena de carácteres: "
salida: .asciiz "\nResultado: "

.text
main:
    # Lectura de dos cadenas
    jal cadenainicio

    # Cadena 1
    li $v0,8
    syscall
    la $a0, buffer
    li $a1, 20 
    move $t1,$a0

    jal cadenainicio

    # Cadena 2
    li $v0,8
    syscall
    la $a0, buffer
    li $a1, 20 
    move $t1,$a0

    jal concatenar

    # Impresión de la cadena salida
    la $a0, salida
    li $v0, 4
    syscall

    # Impresión del resultado
    la $a0, buffer
    move $a0, $t1
    li $v0, 4
    syscall
    b exit

concatenar:
    la $t3, list         # put address of list into $t3
    li $t2, 6            # put the index into $t2
    add $t2, $t2, $t2    # double the index
    add $t2, $t2, $t2    # double the index again (now 4x)
    add $t1, $t2, $t3    # combine the two components of the address
    lw $t4, 0($t1)       # get the value from the array cell
    jr $ra

cadenainicio:
    # Impresión de la cadena inicio
    la $a0, inicio
    li $v0, 4
    syscall
    jr $ra

exit:
    li $v0 10
    syscall
