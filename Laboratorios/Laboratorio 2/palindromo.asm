  .data
 init_msg: .asciiz "Introduzca una palabra, para verificar si es un palindromo: \n" 
 string_largo: .space 1024 
 palindromo_msg: .asciiz "El string ES palindromo.\n"
 no_palindromo_msg: .asciiz "EL string NO es palindromo.\n"
 
 .text
main: 
        li $v0 4
        la $a0 init_msg
        syscall
        
        la $a0, string_largo
        li $a1, 1024
        li $v0, 8
        syscall

        la $t1, string_largo
        la $t2, string_largo
 
length_loop:
        lb $t3, ($t2)
        beqz $t3, end_length_loop
        addu $t2, $t2, 1
        b length_loop

end_length_loop:
        subu $t2, $t2, 2
        
test_loop:
        bge $t1, $t2, es_palindromo
        lb $t3, ($t1)
        lb $t4, ($t2)
        bne $t3, $t4, no_palindromo
        
        addu $t1, $t1, 1
        subu $t2, $t2, 1
        b test_loop

 es_palindromo:
        la $a0, palindromo_msg
        li $v0, 4
        syscall
        b exit

 no_palindromo:
        la $a0, no_palindromo_msg
        li $v0, 4
        syscall
        b exit

 exit:
        li $v0, 10
        syscall 

