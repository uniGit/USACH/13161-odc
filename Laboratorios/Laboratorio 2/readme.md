## Organización de Computadores,  
Laboratorio N2, 01/2019

Se solicita desarrollar los siguientes problemas utilizando el software QtSpim o MARS. Se exige  
que el código se presente de la forma más limpia y legible posible. Puede agregar un archivo  
readme para indicar como implementó su solución.

- [ ] Concatenar: A través de la entrada de usuario se debe solicitar dos strings y el programa debe
ser capaz de concatenarlos y mostrar el resultado final por consola.

- [x] Reverse: A través de la entrada de usuario se debe solicitar un string y el programa debe ser
capaz de mostrar por consola dicho string luego de aplicarle la función reverse.

- [x] Palíndromo: Dado un string ingresado por el usuario se debe determinar si es palíndromo o
no.
