## Organización de Computadores,  
Laboratorio N1, 01/2019

Se solicita desarrollar los siguientes problemas utilizando el software QtSpim o MARS. Se exige  
que el código se presente de la forma más limpia y legible posible. Puede agregar un archivo  
readme para indicar como implementó su solución.

- [x] Calcular la suma de la serie de Lucas para el n-ésimo término. El programa recibirá el valor n.  
La salida corresponde a la suma de todos los términos de la serie hasta el valor de la posición n  
que se ingresó.
- [x] Calcular el factorial de un número n. El programa recibirá el valor n. La salida corresponde al  
resultado del factorial del número ingresado.
- [ ] Obtención del máximo común divisor entre dos números. El programa recibirá dos números  
enteros y positivos con un largo menor a 32 bits en base decimal.