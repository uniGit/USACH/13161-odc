.data
inicio: .asciiz "\n Introdusca un numero para mostrar la serie de lucas: "
salida: .asciiz "\n La serie es: "
espacio: .asciiz " "

.text
main: li $v0 4
      la $a0 inicio
      syscall
      li $v0 5
      syscall
      move $t0 $v0
      li $t1 0
      li $t2 2
      li $t3 -1
      li $t4 2
      
loop: beq $t0 $t1 exit
      li $v0 1
      move $a0 $t2
      syscall
      li $v0 4
      la $a0 espacio
      syscall
      add $t2 $t2 $t3
      move $t3 $t4
      move $t4 $t2
      addi $t1 $t1 1      
      b loop
      
exit: li $v0 10
      syscall                  
      
