# Lectura de dos números
li $v0,1
move $t1,$v0
li $v0,1
move $t2,$v0

# El mayor será el dividendo, el menor, el divisor
slt $t3, $t1, $t2
beq $t3,$0,primeromenor
move $s1, $t1
move $s2, $t2
j restocuociente

primeromenor:
move $s2, $t1
move $s1, $t2
j restocuociente

# El cuociente y resto necesitan empezar como 0
restocuociente:
move $s3, $0
move $s0, $0
j divmod

# Divide y saca el mod restando hasta que el resto sea un valor negativo
divmod:
slt $t0, $s0, $0 # Si resto > 0, t=1, sino, t=0
beq $t0, $0, findividir # Si t = 0, o sea, resto <= 0, findividir
addi $s3, $s3, 1 # Cuociente++
sub $s0, $s1, $s2 # resto = dividendo - divisor
j divmod
# En esta función de dividir, ocurre el desastre y el caos

# Una vez termine de dividir, se compara si el resto es 0
# Si es 0, significa que ese divisor es el máximo común divisor
findividir:
beq $s0, $0, resultado
move $s1, $s2
move $s2, $s0
j divmod

# Se procede a imprimir
resultado:
li $v0,1
move $a0,$s0

# Y luego se termina el programa
syscall
li $v0, 10
syscall
